package com.placetagram.app.repository.auth;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.placetagram.app.model.Result;
import com.placetagram.app.model.User;
import com.placetagram.app.shared.Listener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AuthFirebase {

    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public void login(String email, String password, Listener<Result<FirebaseUser>> listener) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                listener.onComplete(new Result.Success<FirebaseUser>(getLoggedInUser()));
            }
        }).addOnFailureListener(e -> {
            listener.onComplete(new Result.Error("Wrong Credentials"));
        });

    }

    public void register(String name, String email, String password, Listener<Result<FirebaseUser>> listener) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", name);
            map.put("email", email);
            map.put("id", getLoggedInUser().getUid());

            FirebaseUser user = firebaseAuth.getCurrentUser();
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
            user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()) {
                        db.collection("users").document(getLoggedInUser().getUid()).set(map).addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                listener.onComplete(new Result.Success<FirebaseUser>(getLoggedInUser()));
                            }

                        }).addOnFailureListener(e -> {
                            listener.onComplete(new Result.Error("Failed to register user"));
                        });
                    }
                }
            });



//            db.collection("users").document(getLoggedInUser().getUid()).set(map).addOnCompleteListener(task -> {
//                if (task.isSuccessful()) {
//                    listener.onComplete(new Result.Success<String>(getLoggedInUser().getUid()));
//                }
//
//            }).addOnFailureListener(e -> {
//                listener.onComplete(new Result.Error("Failed to register user"));
//            });

        });
    }

    public void logout(Listener<Result<String>> listener) {
        firebaseAuth.signOut();
        listener.onComplete(new Result.Success<String>(null));
    }


    public FirebaseUser getLoggedInUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user;
        } else return null;
    }


    public void getUserById(String id, Listener<Result<User>> listener) {
        db.collection("users").document(id).get().addOnSuccessListener((r) -> {
            Map<String, Object> data = r.getData();
            assert data != null;
            User user = new User(
                    id,
                    Objects.requireNonNull(data.get("email")).toString(),
                    Objects.requireNonNull(data.get("name")).toString()
            );
            listener.onComplete(new Result.Success<User>(user));
        }).addOnFailureListener((e) -> {
            listener.onComplete(new Result.Error("Failed to get user with id = " + id));
        });

    }
}


