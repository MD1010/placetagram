package com.placetagram.app.repository.post;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.placetagram.app.model.AppLocalDb;
import com.placetagram.app.model.Post;
import com.placetagram.app.model.Result;
import com.placetagram.app.shared.AddPostListener;
import com.placetagram.app.shared.Listener;

import java.util.List;

public class PostSql {
    public void addPost(Post post, final AddPostListener listener){
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.postDao().insertAll(post);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if(listener !=null){
                    listener.onComplete();
                }
            }
        };
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }
    public LiveData<List<Post>> getAllPosts(){
        return AppLocalDb.db.postDao().getAllPosts(false);
    }

    public LiveData<List<Post>> getMyPosts(String userID){
        return AppLocalDb.db.postDao().getMyPosts(userID,false);
    }



    public void deletePost(String postId, Listener<Result<Boolean>> listener){
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.postDao().delete(postId);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if(listener !=null){
                    listener.onComplete(new Result.Success<Boolean>(true));
                }
            }
        };
        MyAsyncTask task = new MyAsyncTask();
        task.execute();

    }

    public void editPost(String postID, String descEt, String imageUrl, Listener<Result<Boolean>> listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.postDao().edit(postID, descEt, imageUrl);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if(listener !=null){
                    listener.onComplete(new Result.Success<Boolean>(true));
                }
            }
        };
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }
}
