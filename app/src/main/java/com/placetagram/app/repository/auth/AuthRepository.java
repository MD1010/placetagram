package com.placetagram.app.repository.auth;

import com.google.firebase.auth.FirebaseUser;
import com.placetagram.app.model.Result;
import com.placetagram.app.model.User;
import com.placetagram.app.shared.Listener;

public class AuthRepository {

    private static AuthRepository instance = null;
    public AuthFirebase authFirebase = new AuthFirebase();


    public static AuthRepository getInstance() {
        if (instance != null) {
            return instance;
        } else return new AuthRepository();
    }

    public void login(String email, String password, Listener<Result<FirebaseUser>> listener) {
        authFirebase.login(email, password, listener);
    }

    public void logout(Listener<Result<String>> listener) {
        authFirebase.logout(listener);
    }

    public void register(String name, String email, String password, Listener<Result<FirebaseUser>> listener) {
        authFirebase.register(name, email, password, listener);
    }

    public FirebaseUser getLoggedInUser() {
        return authFirebase.getLoggedInUser();
    }

    public void getUserById(String id, Listener<Result<User>> listener) {
        authFirebase.getUserById(id, listener);

    }


}