package com.placetagram.app.repository.post;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.placetagram.app.model.Post;
import com.placetagram.app.model.Result;
import com.placetagram.app.shared.Listener;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PostFirebase {

    public void getPosts(Long lastUpdated, String userID, Listener<Result<List<Post>>> listener ){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Timestamp ts = new Timestamp(lastUpdated,0);
        if(userID == null) {
            com.google.firebase.firestore.Query first =  db.collection("posts").whereGreaterThanOrEqualTo("lastUpdated",ts);
            com.google.firebase.firestore.Query second =  db.collection("users");

            Task firstTask = first.get();

            Task secondTask = second.get();

            Tasks.whenAllSuccess(firstTask, secondTask).addOnSuccessListener(objects -> {
                List<Post> data = new LinkedList<Post>();
                List<Object> posts = ((QuerySnapshot) objects.get(0)).toObjects(Object.class);
                List<Object> users = ((QuerySnapshot) objects.get(1)).toObjects(Object.class);
                for(int i =0; i < posts.size(); i ++) {
                    int userIndex = 0;
                    for(int j=0; j < users.size(); j++) {
                        if(((HashMap) posts.get(i)).get("user").equals(((HashMap) users.get(j)).get("id")))  {
                            userIndex = j;
                            break;
                        }
                    }
                    Post post = new Post();
                    post.fromMap((Map<String, Object>) posts.get(i));
                    post.setUser((String) ((HashMap) users.get(userIndex)).get("name"));
                    data.add(post);
                }
                listener.onComplete(new Result.Success<List<Post>>(data));
            });
        } else {
            com.google.firebase.firestore.Query first =  db.collection("posts").whereGreaterThanOrEqualTo("lastUpdated",ts).whereEqualTo("user", userID);
            com.google.firebase.firestore.Query second =  db.collection("users");
          Task firstTask = first.get();

          Task secondTask = second.get();

          Tasks.whenAllSuccess(firstTask, secondTask).addOnSuccessListener(objects -> {
              List<Post> data = new LinkedList<Post>();
              List<Object> posts = ((QuerySnapshot) objects.get(0)).toObjects(Object.class);
              List<Object> users = ((QuerySnapshot) objects.get(1)).toObjects(Object.class);
              for(int i =0; i < posts.size(); i ++) {
                  int userIndex = 0;
                  for(int j=0; j < users.size(); j++) {
                    if(((HashMap) posts.get(i)).get("user").equals(((HashMap) users.get(j)).get("id")))  {
                        userIndex = j;
                        break;
                    }
                  }
                  Post post = new Post();
                  post.fromMap((Map<String, Object>) posts.get(i));
                  post.setUser((String) ((HashMap) users.get(userIndex)).get("name"));
                  data.add(post);
              }
              listener.onComplete(new Result.Success<List<Post>>(data));
          });
        }
    }

    public void uploadImage(Bitmap imageBmp, String name, Listener<Result<String>> listener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference imagesRef = storage.getReference().child("images").child(name);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.onComplete(null);
            }
        }).addOnSuccessListener(taskSnapshot -> imagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
            Uri downloadUrl = uri;
            listener.onComplete( new Result.Success<String>(downloadUrl.toString()));
        }));
    }


    public void addPost(Post post, Listener<Result<Boolean>> listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name

        // Add a new document with a generated ID
        db.collection("posts").document(post.getId())
                .set(post.toMap()).addOnSuccessListener(aVoid -> {
                    Log.d("TAG", "post added successfully");
                    listener.onComplete(new Result.Success<Boolean>(true));
                }).addOnFailureListener(e -> {
                    Log.d("TAG", "fail adding post");
                    listener.onComplete(new Result.Success<Boolean>(false));

                });
    }

    public void editPost(String postID, String descEt, String imageUrl, Listener<Result<Boolean>> listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        HashMap<String, Object> updatePost = new HashMap<>();
        updatePost.put("description", descEt);
        updatePost.put("postImage", imageUrl);
        updatePost.put("lastUpdated", FieldValue.serverTimestamp());

        db.collection("posts").document(postID).update(updatePost).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "post edited successfully");
            listener.onComplete(new Result.Success<Boolean>(true));
        }).addOnFailureListener(e -> {
            Log.d("TAG", "fail edit post");
            listener.onComplete(new Result.Success<Boolean>(false));

        });
    }

    public void deletePost(String postID, Listener<Result<Boolean>> listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> docData = new HashMap<>();
        docData.put("isDeleted", true);
        docData.put("lastUpdated", FieldValue.serverTimestamp());

        db.collection("posts").document(postID).update(docData).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "post deleted successfully");
            listener.onComplete(new Result.Success<Boolean>(true));
        }).addOnFailureListener(e -> {
            Log.d("TAG", "fail deleted post");
            listener.onComplete(new Result.Success<Boolean>(false));

        });
    }
}
