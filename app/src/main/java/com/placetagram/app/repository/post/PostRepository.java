package com.placetagram.app.repository.post;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.placetagram.app.MyApplication;
import com.placetagram.app.model.Post;
import com.placetagram.app.model.Result;

import com.placetagram.app.shared.GetAllPostsListener;
import com.placetagram.app.shared.Listener;

import java.util.List;

public class PostRepository {
    private static final PostRepository instance = null;
    public PostFirebase postFirebase = new PostFirebase();
    public PostSql postSql = new PostSql();


    public static PostRepository getInstance() {
        return new PostRepository();
    }

    public void addPost(Post post, Bitmap imageBmp, String name, Listener<Result<Boolean>> listener) {
        postFirebase.uploadImage(imageBmp, name, url -> {
            if (url instanceof Result.Success) {
                post.setPostImage((String) ((Result.Success) url).getData());
                postFirebase.addPost(post, listener);
            }
        });
    }

    public void editPost(String postID, String descEt, Bitmap imageBmp, Listener<Result<Boolean>> listener) {
        postFirebase.uploadImage(imageBmp, postID, url -> {
            if (url instanceof Result.Success) {
                //post.setPostImage((String) ((Result.Success) url).getData());
                String imageUrl = (String) ((Result.Success) url).getData();
                postFirebase.editPost(postID, descEt, imageUrl, listener);
                postSql.editPost(postID, descEt, imageUrl, listener);
            }
        });
        //postFirebase.editPost(postID, descEt, listener);
    }

    public void deletePost(String postID,Listener<Result<Boolean>> listener) {
        postFirebase.deletePost(postID, listener);
        postSql.deletePost(postID, listener);
    }

    public void refreshAllPosts(final GetAllPostsListener listener,String userID){
        SharedPreferences sp = MyApplication.context.getSharedPreferences("TAG",Context.MODE_PRIVATE);
        Long lastUpdated = sp.getLong("lastUpdated",0);

        postFirebase.getPosts(lastUpdated,userID, result -> {
            long lastU = 0;
            for (Post p: (List<Post>) ((Result.Success) result).getData()){
                postSql.addPost(p,null);
                if (p.getLastUpdated() > lastU){
                    lastU = p.getLastUpdated();
                }
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putLong("lastUpdated",lastU);
            editor.commit();
            if (listener !=null){
                listener.onComplete();
            }

        });

    }
}
