package com.placetagram.app;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.placetagram.app.ui.auth.AuthViewModel;

public class MainActivity extends AppCompatActivity {

    AuthViewModel authViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigationView =  findViewById(R.id.bottomNavigationView);
//        navigationView.setVisibility(View.INVISIBLE);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.postsListFragment, R.id.mapsFragment)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.main_nav_host);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
////        getMenuInflater().inflate(R.menu.main_menu, menu);
//        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
//        authViewModel.getLoggedInUser().observe(this, user -> {
//            if (user == null) {
//                findNavController(this,R.id.main_nav_host).navigate(R.id.loginFragment);
//            }
//        });
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.logout_btn) {
//            authViewModel.logout();
//        }
//        return super.onOptionsItemSelected(item);
//    }


}