package com.placetagram.app.ui.auth;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseUser;
import com.placetagram.app.model.Result;
import com.placetagram.app.model.User;
import com.placetagram.app.repository.auth.AuthRepository;
import com.placetagram.app.shared.Listener;

import static com.placetagram.app.MyApplication.context;

public class AuthViewModel extends ViewModel {

    private final AuthRepository authRepository = AuthRepository.getInstance();
    private final MutableLiveData<FirebaseUser> loggedInUser = new MutableLiveData<>(authRepository.getLoggedInUser());


    private final MutableLiveData<String> authError = new MutableLiveData<>(null);


    public AuthViewModel() {
    }

    public LiveData<FirebaseUser> getLoggedInUser() {
        return loggedInUser;
    }



    public LiveData<String> getAuthError() {
        return authError;
    }

    public void login(String email, String password) {
        authRepository.login(email, password, this::handleAuthResult);
    }

    public void logout() {
        authRepository.logout(this::handleAuthResult);
    }

    public void register(String name, String email, String password) {
        authRepository.register(name, email, password, this::handleAuthResult);
    }

    public void getUserById(String id, Listener<Result<User>> listener) {
        authRepository.getUserById(id, listener);

    }

    private void handleAuthResult(Result result) {
        if (result instanceof Result.Success) {
            loggedInUser.setValue((FirebaseUser) ((Result.Success) result).getData());
        } else if (result instanceof Result.Error) {
            authError.setValue(((Result.Error) result).getError());
        }
    }


    public void showAuthFailedError(String error) {
        if (context != null && error != null) {
            Toast.makeText(
                    context,
                    error,
                    Toast.LENGTH_SHORT).show();
        }
    }
}