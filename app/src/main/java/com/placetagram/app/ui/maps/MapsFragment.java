package com.placetagram.app.ui.maps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.security.keystore.KeyNotYetValidException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.placetagram.app.R;
import com.placetagram.app.model.Post;
import com.placetagram.app.shared.GetAllPostsListener;
import com.placetagram.app.ui.post.MyPostsFragmentDirections;
import com.placetagram.app.ui.post.PostListViewModel;

import java.util.HashMap;
import java.util.List;

public class MapsFragment extends Fragment {
    private GoogleMap mMap;
    List<Post> postList;
    HashMap<Marker, Post> markerToPost = new HashMap<Marker, Post>();
    PostListViewModel postViewModel;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.VISIBLE);
         view = inflater.inflate(R.layout.fragment_maps, container, false);
        postViewModel = new ViewModelProvider(this).get(PostListViewModel.class);
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.google_map);

        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                addMarkersToMap();
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {

                    }

                });
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        Post currentPost = markerToPost.get(marker);
                        MapsFragmentDirections.ActionMapsFragmentToPostFromMapFragment action =
                                MapsFragmentDirections.actionMapsFragmentToPostFromMapFragment(currentPost.getUser(),
                                        currentPost.getPostImage(),currentPost.getDescription());
                        Navigation.findNavController(view).navigate(action);


                        return false;
                    }
                });
            }

            private void addMarkersToMap() {

                     postViewModel.getPostList().observe(getViewLifecycleOwner(), posts -> {
                         postList = posts;
                         for (Post post:postList
                         ) {

                             Marker marker = mMap.addMarker(new MarkerOptions()
                                     .position(new LatLng(post.getLatitude(),post.getLongitude()))
                                     .title(post.getUser()).snippet(post.getDescription())
                                        );

                             markerToPost.put(marker,post);

                         }

                     });
                 }

        });
        return view;
    }
}