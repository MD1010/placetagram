package com.placetagram.app.ui.auth;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.placetagram.app.R;
import com.placetagram.app.shared.Helpers;

public class RegisterFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.INVISIBLE);
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AuthViewModel authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        final EditText emailEditText = view.findViewById(R.id.email);
        final EditText passwordEditText = view.findViewById(R.id.password);
        final EditText nameEditText = view.findViewById(R.id.name);
        final Button registerButton = view.findViewById(R.id.register_btn);
        final TextView existingUserLink = view.findViewById(R.id.existing_user);
        final ProgressBar loadingProgressBar = view.findViewById(R.id.loading);


        authViewModel.getLoggedInUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                Navigation.findNavController(getView()).navigate(R.id.action_registerFragment_to_postsListFragment);
            }
            loadingProgressBar.setVisibility(View.INVISIBLE);
        });

        authViewModel.getAuthError().observe(getViewLifecycleOwner(), error -> {
            loadingProgressBar.setVisibility(View.INVISIBLE);
            authViewModel.showAuthFailedError(error);
        });

        existingUserLink.setOnClickListener(v -> {
            Navigation.findNavController(getView()).popBackStack();
        });

        registerButton.setOnClickListener(v -> {
            Helpers.hideSoftKeyboard(getActivity());
            String passwordTxt = passwordEditText.getText().toString();
            String emailTxt = emailEditText.getText().toString();
            String nameTxt = nameEditText.getText().toString();

            if (TextUtils.isEmpty(nameTxt)
                    || TextUtils.isEmpty(emailTxt) || TextUtils.isEmpty(passwordTxt)) {
                authViewModel.showAuthFailedError("Empty credentials!");
            } else if (!Helpers.isEmailValid(emailTxt)) {
                authViewModel.showAuthFailedError("Invalid Email!");
            } else if (passwordTxt.length() < 6) {
                authViewModel.showAuthFailedError("Password too short!");
            } else {
                loadingProgressBar.setVisibility(View.VISIBLE);
                authViewModel.register(nameTxt, emailTxt, passwordTxt);
            }
        });
    }

}