package com.placetagram.app.ui.post;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.placetagram.app.model.Post;
import com.placetagram.app.model.Result;
import com.placetagram.app.repository.post.PostRepository;
import com.placetagram.app.repository.post.PostSql;
import com.placetagram.app.shared.GetAllPostsListener;

import java.util.List;

public class PostListViewModel extends ViewModel {

    public PostSql postSql = new PostSql();
    private final PostRepository postRepository = PostRepository.getInstance();

    LiveData<List<Post>> postList;
    LiveData<List<Post>> myPostList;

    public LiveData<List<Post>> getPostList() {
        if (postList == null){
            postList = postSql.getAllPosts();
            System.out.println(1);
        }
        return postList;
    }

    public LiveData<List<Post>> getMyPostList(String userID) {
        if (myPostList == null){
            myPostList = postSql.getMyPosts(userID);
        }
        return myPostList;
    }


    private final MutableLiveData<Boolean> isPostAdded = new MutableLiveData<Boolean>();
    private final MutableLiveData<Boolean> isPostUpdated = new MutableLiveData<Boolean>();
    private final MutableLiveData<Boolean> isPostDeleted = new MutableLiveData<Boolean>();

    public LiveData<Boolean> getIsPostAdded() {
        return isPostAdded;
    }

    public LiveData<Boolean> getIsPostUpdated() {
        return isPostUpdated;
    }

    public LiveData<Boolean> getIsPostDeleted() {
        return isPostDeleted;
    }

    public void refreshAllPosts(final GetAllPostsListener listener, String userID){
        postRepository.refreshAllPosts(listener, userID);
    }

    public void addPost(Post post, Bitmap imageBmp, String name) {
        postRepository.addPost(post, imageBmp, name, result -> {
            if (result instanceof Result.Success) {
                isPostAdded.setValue((Boolean) ((Result.Success) result).getData());
            }
        } );
    }

    public void deletePost(String postID) {
        postRepository.deletePost(postID, result -> {
            if (result instanceof Result.Success) {
                isPostDeleted.setValue((Boolean) ((Result.Success) result).getData());
            }
        } );
    }

    public void editPost(String postID, String descEt, Bitmap imageBmp) {
        postRepository.editPost(postID,descEt, imageBmp, result -> {
            if (result instanceof Result.Success) {
                isPostUpdated.setValue((Boolean) ((Result.Success) result).getData());
            }
        } );
    }


}
