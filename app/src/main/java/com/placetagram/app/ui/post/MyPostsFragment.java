package com.placetagram.app.ui.post;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.placetagram.app.MyApplication;
import com.placetagram.app.R;
import com.placetagram.app.model.Post;
import com.placetagram.app.shared.GetAllPostsListener;
import com.placetagram.app.ui.auth.AuthViewModel;
import com.squareup.picasso.Picasso;

import static androidx.navigation.Navigation.findNavController;


public class MyPostsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    PostListViewModel postViewModel;
    ProgressBar progressBar;
    AuthViewModel authViewModel;
    MyPostsRecycleViewAdapter postsAdapter;
    TextView empty_posts;
    Button logOutButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_posts, container, false);

        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.VISIBLE);


        postViewModel = new ViewModelProvider(this).get(PostListViewModel.class);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        RecyclerView postsRecycleView = view.findViewById(R.id.myrecyclerView);
        progressBar = view.findViewById(R.id.mypostlist_progress);
        logOutButton = view.findViewById(R.id.logOutButton);
        logOutButton.setOnClickListener(v -> {
            authViewModel.logout();
        });
        progressBar.setVisibility(View.INVISIBLE);
        empty_posts = view.findViewById(R.id.empty_my_post_list);
        empty_posts.setVisibility(View.INVISIBLE);

        postsRecycleView.hasFixedSize();
        LinearLayoutManager layoutManager = new LinearLayoutManager(MyApplication.context);
        postsRecycleView.setLayoutManager(layoutManager);


        postsAdapter = new MyPostsRecycleViewAdapter();
        postsRecycleView.setAdapter(postsAdapter);
        progressBar.setVisibility(View.VISIBLE);
        postViewModel.getMyPostList(authViewModel.getLoggedInUser().getValue().getDisplayName()).observe(getViewLifecycleOwner(), posts -> {
            postsAdapter.notifyDataSetChanged();
            if (posts != null) {
                if (posts.size() == 0) {
                    empty_posts.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.INVISIBLE);
            } else {
//                empty_posts.setVisibility(View.VISIBLE);
                // create text view , set it visibilty and in the beggining set it text to be "There is no posts ... :("
            }
        });

        authViewModel.getLoggedInUser().observe(getViewLifecycleOwner(), user -> {
            if (user == null) {
                findNavController(getActivity(), R.id.main_nav_host).navigate(R.id.loginFragment);
            }
        });

        postViewModel.getIsPostDeleted().observe(getViewLifecycleOwner(), isPostDeleted -> {
            if (isPostDeleted) {
                reloadData();
            } else {
                displayFailedError();
            }
        });

        reloadData();
        return view;
    }

    private void displayFailedError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Operation Failed");
        builder.setMessage("delete post failed, please try again later...");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    void reloadData() {
        progressBar.setVisibility(View.VISIBLE);
        postViewModel.refreshAllPosts(new GetAllPostsListener() {
            @Override
            public void onComplete() {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }, authViewModel.getLoggedInUser().getValue().getDisplayName());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    class MyPostsViewHolder extends RecyclerView.ViewHolder {
        TextView postAuthor;
        TextView postDescription;
        ImageView postImage;
        ImageView editPostBtn, deletePostBtn;


        public MyPostsViewHolder(@NonNull View itemView) {
            super(itemView);
            postAuthor = itemView.findViewById(R.id.authorName);
            postImage = itemView.findViewById(R.id.specific_post_post_img);
            postDescription = itemView.findViewById(R.id.postDescription);
            editPostBtn = itemView.findViewById(R.id.edit_post_btn);
            deletePostBtn = itemView.findViewById(R.id.delete_post_btn);
        }
    }

    public class MyPostsRecycleViewAdapter extends RecyclerView.Adapter<MyPostsViewHolder> {

        @NonNull
        @Override
        public MyPostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.specific_post, parent, false);
            MyPostsViewHolder holder = new MyPostsViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyPostsViewHolder holder, int position) {

            Post post = postViewModel.getMyPostList(authViewModel.getLoggedInUser().getValue().getDisplayName()).getValue().get(position);
            holder.postAuthor.setText("@" + post.getUser());
            holder.postDescription.setText(post.getDescription());


            holder.editPostBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MyPostsFragmentDirections.ActionMyPostsFragmentToEditPostFragment action = MyPostsFragmentDirections.actionMyPostsFragmentToEditPostFragment(post.getId(), post.getDescription(), post.getPostImage());
                    Navigation.findNavController(view).navigate(action);

                }

            });


            holder.deletePostBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postViewModel.deletePost(post.getId());


                }

            });

            holder.postImage.setImageResource(R.drawable.avatar);
            if (post.getPostImage() != null) {
                Picasso.get().load(post.getPostImage()).placeholder(R.drawable.avatar).into(holder.postImage);
            }

        }

        @Override
        public int getItemCount() {

            if (postViewModel.getMyPostList(authViewModel.getLoggedInUser().getValue().getDisplayName()).getValue() == null) {
                return 0;
            }
            return postViewModel.getMyPostList(authViewModel.getLoggedInUser().getValue().getDisplayName()).getValue().size();
        }
    }
}