package com.placetagram.app.ui.post;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.placetagram.app.R;
import com.squareup.picasso.Picasso;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class EditPostFragment extends Fragment {
    String postID, postImageUrl, postDesription;
    EditText descEt;
    Button saveBtn;
    PostListViewModel postViewModel;
    ProgressBar progressBar;
    ImageView postImageView;
    ImageButton editImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_edit_post, container, false);
        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.INVISIBLE);
        postViewModel = new ViewModelProvider(this).get(PostListViewModel.class);
        progressBar = view.findViewById(R.id.edit_post_progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        postImageView = view.findViewById(R.id.edit_post_avatar_imv);
        editImage = view.findViewById(R.id.editpost_edit_image_btn);
        editImage.setOnClickListener(view1 -> editImage());
        postID = EditPostFragmentArgs.fromBundle(getArguments()).getPostID();
        descEt = view.findViewById(R.id.edit_description);
        descEt.setText(EditPostFragmentArgs.fromBundle(getArguments()).getDescription());
        saveBtn = view.findViewById(R.id.edit_post_save_btn);
        Picasso.get().load(EditPostFragmentArgs.fromBundle(getArguments()).getImageUrl()).placeholder(R.drawable.avatar).into(postImageView);
        //saveCurrentPicture(EditPostFragmentArgs.fromBundle(getArguments()).getImageUrl());
        Log.d("fdfdf", "onCreateView: " + postID);

        postViewModel.getIsPostUpdated().observe(getViewLifecycleOwner(), isUpdatePost -> {
            if (isUpdatePost) {
                progressBar.setVisibility(View.INVISIBLE);
                Navigation.findNavController(saveBtn).popBackStack();
            } else {
                displayFailedError();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                saveBtn.setEnabled(false);
                editPost();
            }

        });



        descEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                saveBtn.setEnabled(s.toString().trim().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    private void saveCurrentPicture(String imageUrl) {
        try {
//            Picasso.get().load(imageUrl).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    // loaded bitmap is here (bitmap)
//                    postImageView.setImageBitmap(bitmap);
//                }
//
//                @Override
//                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//                    Log.d("picaso-dov", "onBitmapFailed: " + e);
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {}
//            });
        } catch(Exception e) {
            System.out.println(e);
        }

    }


    private void editPost() {
        BitmapDrawable drawable = (BitmapDrawable)postImageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        postViewModel.editPost(postID, descEt.getText().toString(), bitmap);
    }


    private void editImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        postImageView.setImageBitmap(selectedImage);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                postImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void displayFailedError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Operation Failed");
        builder.setMessage("Updated post failed, please try again later...");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}