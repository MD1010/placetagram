package com.placetagram.app.ui.post;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.placetagram.app.MyApplication;
import com.placetagram.app.R;
import com.placetagram.app.model.Post;
import com.placetagram.app.shared.GetAllPostsListener;
import com.placetagram.app.ui.auth.AuthViewModel;
import com.squareup.picasso.Picasso;


public class PostsListFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    PostListViewModel postViewModel;
    ProgressBar progressBar;
    AuthViewModel authViewModel;
    PostsRecycleViewAdapter postsAdapter;
    SwipeRefreshLayout sref;

    FloatingActionButton addPostBtn;
    String postType = "All Posts";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_posts_list, container, false);
        Log.d("db path", "onCreateView: " + getContext().getDatabasePath("dbFileName.db"));
        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.VISIBLE);
        BottomNavigationView navigationView =  ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView));
        int id = navigationView.getSelectedItemId();
        MenuItem selectedItem = navigationView.getMenu().findItem(id);

        Log.d("id", "bottom id is: " +  selectedItem.getTitle());
        postViewModel = new ViewModelProvider(this).get(PostListViewModel.class);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        RecyclerView postsRecycleView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.postlist_progress);
        progressBar.setVisibility(View.INVISIBLE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.posts_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        postsRecycleView.hasFixedSize();
        LinearLayoutManager layoutManager = new LinearLayoutManager(MyApplication.context);
        postsRecycleView.setLayoutManager(layoutManager);

        sref = view.findViewById(R.id.studentslist_swipe);
        sref.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sref.setRefreshing(true);
                reloadData();

            }
        });
        postsAdapter = new PostsRecycleViewAdapter();
        postsRecycleView.setAdapter(postsAdapter);
        progressBar.setVisibility(View.VISIBLE);
        postViewModel.getPostList().observe(getViewLifecycleOwner(), posts -> {
            postsAdapter.notifyDataSetChanged();
            if (posts != null) {
                progressBar.setVisibility(View.INVISIBLE);
            }else{
                // create text view , set it visibilty and in the beggining set it text to be "There is no posts ... :("
            }
        });
        postViewModel.getIsPostDeleted().observe(getViewLifecycleOwner(), isPostDeleted -> {
            if (isPostDeleted) {
                reloadData();
            } else {
                displayFailedError();
            }
        });
        addPostBtn = (FloatingActionButton) view.findViewById(R.id.postlist_add_btn);
        addPostBtn.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_postsListFragment_to_addPostFragment));

        reloadData();
        return view;
    }

    private void displayFailedError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Operation Failed");
        builder.setMessage("delete post failed, please try again later...");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    void reloadData(){

        progressBar.setVisibility(View.VISIBLE);
        postViewModel.refreshAllPosts(new GetAllPostsListener() {
            @Override
            public void onComplete() {
                sref.setRefreshing(false);
            }
        }, null);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String text = adapterView.getItemAtPosition(i).toString();
        Log.d("spinner", "onItemSelected: " + text);
        postType = text;
        reloadData();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    class PostsViewHolder extends RecyclerView.ViewHolder{
        TextView postAuthor;
        TextView postDescription;
        ImageView postImage;
        ImageView editPostBtn, deletePostBtn;


        public PostsViewHolder(@NonNull View itemView) {
            super(itemView);
            postAuthor = itemView.findViewById(R.id.authorName);
            postImage = itemView.findViewById(R.id.specific_post_post_img);
            postDescription = itemView.findViewById(R.id.postDescription);
            editPostBtn = itemView.findViewById(R.id.edit_post_btn);
            deletePostBtn = itemView.findViewById(R.id.delete_post_btn);


        }
    }

    public class PostsRecycleViewAdapter extends RecyclerView.Adapter<PostsViewHolder> {

        @NonNull
        @Override
        public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.specific_post,parent,false);
            PostsViewHolder holder = new PostsViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull PostsViewHolder holder, int position) {

            Post post = postViewModel.getPostList().getValue().get(position);
            holder.postAuthor.setText("@"+post.getUser());

            holder.postDescription.setText(post.getDescription());
            Log.d("fdf", "onBindViewHolder: auth user: " + authViewModel.getLoggedInUser().getValue() + " post user: " + post.getUser());
            holder.editPostBtn.setVisibility(View.INVISIBLE);

            holder.editPostBtn.setVisibility(View.INVISIBLE);


            holder.deletePostBtn.setVisibility(View.INVISIBLE);


            holder.postImage.setImageResource(R.drawable.avatar);
            if (post.getPostImage() != null){
                Picasso.get().load(post.getPostImage()).placeholder(R.drawable.avatar).into(holder.postImage);
            }
        }

        @Override
        public int getItemCount() {
            if (postViewModel.getPostList().getValue() == null){
                return 0;
            }
            return postViewModel.getPostList().getValue().size();
        }
    }

}