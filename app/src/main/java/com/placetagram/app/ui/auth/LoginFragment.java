package com.placetagram.app.ui.auth;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.placetagram.app.R;
import com.placetagram.app.shared.Helpers;

public class LoginFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.INVISIBLE);
        return inflater.inflate(R.layout.fragment_login, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AuthViewModel authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        if(authViewModel.getLoggedInUser().getValue() != null){
                Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_postsListFragment);
        }
        final EditText emailEditText = view.findViewById(R.id.email);
        final EditText passwordEditText = view.findViewById(R.id.password);
        final Button loginButton = view.findViewById(R.id.login_btn);
        final ProgressBar loadingProgressBar = view.findViewById(R.id.loading);
        final TextView registerLink = view.findViewById(R.id.new_user_register);


        authViewModel.getLoggedInUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_postsListFragment);
                Log.d("Tag", user.getEmail());
            }
            loadingProgressBar.setVisibility(View.INVISIBLE);
        });


        authViewModel.getAuthError().observe(getViewLifecycleOwner(), error -> {
            loadingProgressBar.setVisibility(View.INVISIBLE);
            authViewModel.showAuthFailedError(error);
        });


        registerLink.setOnClickListener( v-> {
            Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_registerFragment);
        });

        loginButton.setOnClickListener(v -> {
            Helpers.hideSoftKeyboard(getActivity());
            String passwordTxt = passwordEditText.getText().toString();
            String emailTxt = emailEditText.getText().toString();


            if (TextUtils.isEmpty(passwordTxt) || TextUtils.isEmpty(emailTxt)) {
                authViewModel.showAuthFailedError("Empty credentials!");

            } else {
                loadingProgressBar.setVisibility(View.VISIBLE);
                authViewModel.login(emailEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }


        });
    }

}