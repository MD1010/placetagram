package com.placetagram.app.ui.post;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.net.Uri;
import android.os.Bundle;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.location.LocationServices;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.placetagram.app.R;
import com.placetagram.app.model.Post;


import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.placetagram.app.ui.auth.AuthViewModel;


public class AddPostFragment extends Fragment {

    PostListViewModel postViewModel;
    AuthViewModel authViewModel;
    ImageView avatarImageView;
    ImageButton editImage;
    EditText descEt;
    Button submitBtn;
    ProgressBar progressBar;

    FusedLocationProviderClient fusedLocationClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_post, container, false);
        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.INVISIBLE);
        progressBar = view.findViewById(R.id.save_progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        postViewModel = new ViewModelProvider(this).get(PostListViewModel.class);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        avatarImageView = view.findViewById(R.id.add_post_avatar_imv);
        editImage = view.findViewById(R.id.addpost_edit_image_btn);
        descEt = view.findViewById(R.id.addpost_description_et);

        submitBtn = view.findViewById(R.id.addpost_submit_btn);

        editImage.setOnClickListener(view1 -> editImage());
        postViewModel.getIsPostAdded().observe(getViewLifecycleOwner(), isAddPost -> {
            if (isAddPost) {
                progressBar.setVisibility(View.INVISIBLE);
                Navigation.findNavController(submitBtn).popBackStack();
            } else {
                displayFailedError();
            }
        });


        submitBtn.setEnabled(false);
        descEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                submitBtn.setEnabled(s.toString().trim().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Check permission
                if(ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // when permission granted
                    progressBar.setVisibility(View.VISIBLE);
                    submitBtn.setEnabled(false);
                    getLocation();
                } else {
                    // when permission denied
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
                }
            }

            private void getLocation() {
                fusedLocationClient.getLastLocation().addOnCompleteListener(task -> {
                    // Intialize location
                    Location location = task.getResult();
                    if(location != null) {

                        try {
                            // Intialize geocoder
                            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

                            // Intialize address list
                            List<Address> addresses = geocoder.getFromLocation(
                                    location.getLatitude(), location.getLongitude(), 1
                            );
                            Log.d("tag", "latitude is " + addresses.get(0).getLatitude() + " longitude is " + addresses.get(0).getLongitude()
                                    + " country is " + addresses.get(0).getCountryName() + " city is " + addresses.get(0).getLocality());
                            savePost(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        // Initailze fusedLocationClient
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        return view;
    }


    private void savePost(Double latitude, Double longitude) {
        final Post post = new Post();
        post.setDescription(descEt.getText().toString());
        post.setUser(authViewModel.getLoggedInUser().getValue().getUid());
        post.setLongitude(longitude);
        post.setLatitude(latitude);
        //post.setUser("dov");
        post.setDeleted(false);
        post.setId(UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());


        BitmapDrawable drawable = (BitmapDrawable)avatarImageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        postViewModel.addPost(post, bitmap, post.getId());


//        Model.instance.uploadImage(bitmap, post.getId(), url -> {
//            if (url == null){
//                displayFailedError();
//            }else{
//                post.setPostImage(url);
//                Model.instance.addPost(post, () -> Navigation.findNavController(submitBtn).popBackStack());
//            }
//        });
    }

    private void displayFailedError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Operation Failed");
        builder.setMessage("Saving image failed, please try again later...");
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void editImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        avatarImageView.setImageBitmap(selectedImage);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                avatarImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }
}