package com.placetagram.app.ui.post;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.placetagram.app.R;
import com.squareup.picasso.Picasso;


public class PostFromMapFragment extends Fragment {

    private String userName;
    private String imageUrl;
    private String description;


    public PostFromMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userName =  PostFromMapFragmentArgs.fromBundle(getArguments()).getUserName();
        imageUrl = PostFromMapFragmentArgs.fromBundle(getArguments()).getImageUrl();
        description = PostFromMapFragmentArgs.fromBundle(getArguments()).getDescription();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_post_from_map, container, false);
        ((AppCompatActivity) getActivity()).findViewById((R.id.bottomNavigationView)).setVisibility(View.INVISIBLE);
        TextView authorName = view.findViewById(R.id.postMapAuthorName);
        ImageView postImage = view.findViewById(R.id.postMapImage);
        TextView descriptionElement = view.findViewById(R.id.postMapDescription);
        authorName.setText("@"+userName);
        postImage.setImageResource(R.drawable.avatar);
        if (postImage != null){
            Picasso.get().load(imageUrl).placeholder(R.drawable.avatar).into(postImage);
        }
        descriptionElement.setText(description);

        return view;
    }
}