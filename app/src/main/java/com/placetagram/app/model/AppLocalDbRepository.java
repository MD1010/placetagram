package com.placetagram.app.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Post.class}, version = 10)
public abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract PostDao postDao();
}
