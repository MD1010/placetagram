package com.placetagram.app.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;

import java.util.HashMap;
import java.util.Map;

@Entity
public class Post {
    @PrimaryKey
    @NonNull
    //Todo add longtitude and atitude!!!!
    private String id;
    private String user;
    private String userImage;
    private String postImage;
    private String description;
    private Double longitude;
    private Double latitude;
    private Long lastUpdated;
    private Boolean isDeleted;

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("user", user);
        result.put("userImage", userImage);
        result.put("postImage", postImage);
        result.put("description", description);
        result.put("longitude", longitude);
        result.put("latitude", latitude);
        result.put("lastUpdated", FieldValue.serverTimestamp());
        result.put("isDeleted", isDeleted);
        return result;
    }

    public void fromMap(Map<String, Object> map){
        id = (String)map.get("id");
        user = (String)map.get("user");
        userImage = (String)map.get("userImage");
        postImage = (String)map.get("postImage");
        description = (String)map.get("description");
        longitude = (Double)map.get("longitude");
        latitude = (Double)map.get("latitude");
        Timestamp ts = (Timestamp)map.get("lastUpdated");
        lastUpdated = ts.getSeconds();
        isDeleted = (Boolean) map.get("isDeleted");
        //long time = ts.toDate().getTime();
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }


    public Double getLongitude(){
        return this.longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    public Double getLatitude(){
        return this.latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    public void setId(String id) {
        this.id = id;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDescription(String description){this.description=description;}

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getUser() {
        return user;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
