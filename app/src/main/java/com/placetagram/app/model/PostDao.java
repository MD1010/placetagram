package com.placetagram.app.model;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PostDao {

    @Query("select * from Post WHERE isDeleted = :isDeleted")
    LiveData<List<Post>> getAllPosts(Boolean isDeleted);

    @Query("select * from Post WHERE user = :userID and isDeleted = :isDeleted")
    LiveData<List<Post>> getMyPosts(String userID, Boolean isDeleted);

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertAll(Post... posts);

    @Query("DELETE FROM Post WHERE id = :postId")
    void delete(String postId);

    @Query("UPDATE Post SET description = :descEt, postImage = :imageUrl WHERE id = :postID")
    void edit(String postID, String descEt, String imageUrl);

}
