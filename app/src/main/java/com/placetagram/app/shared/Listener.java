package com.placetagram.app.shared;

public interface Listener<T> {
    void onComplete(T result);
}
